#ImportModule
import mysql.connector
import xml.etree.cElementTree as ET
from mysql.connector.cursor import MySQLCursor
from tkinter import *
from lxml import etree

#ConnexionBDD
mydb = mysql.connector.connect(
host="localhost",
user="SuperUtilisateur",
password="",
database="e4")

def connexionBDD():
        curseur = mydb.cursor()
        curseur.execute("SELECT id FROM test")
        print(curseur.fetchall())
        mydb.close()

#Close the program
def close_window(): 
    fenetre.destroy()

#Export and create the file .xml
def export_xml():
        curseur = mydb.cursor()
        curseur.execute("SELECT id, nom, code_postal FROM test")
        ida = curseur.fetchall()

        root = ET.Element("dataroot")
        for id, nom, code_postal in ida:
                doc = ET.SubElement(root, "COMMUNE")
                ET.SubElement(doc, "Com_ID").text = "{}".format(id)
                ET.SubElement(doc, "Com_Nom").text = "{}".format(nom)
                ET.SubElement(doc, "Com_CodePostal").text = "{}".format(code_postal) 
                tree = ET.ElementTree(root)
                tree.write("export.xml")
                mydb.commit()
                labelExport.pack()  
        mydb.close()  


#Call the connexion with BDD, file  xml, take the data of file XML and insert with request SQL in BDD
def import_xml(): 
        global entryfile, entrytable
        entryfield = entryfile.get() 
        table = entrytable.get()
        mycursor = mydb.cursor()
        filexml = etree.parse("{}.xml".format(entryfield))
        for ID, nom, CodePostal in filexml.xpath("/dataroot/COMMUNE"):
                mycursor.execute("insert into {} (id, nom, code_postal) values ({},'{}',{})".format(table,ID.text,nom.text, CodePostal.text ))
                mydb.commit()
                labelImport.pack()  
        mydb.close()      

#Create a fonction for select id from 
def selectid():
        curseur = mydb.cursor()
        curseur.execute("SELECT id FROM test")
        id = curseur.fetchall()
        return id

#Create a fonction for select nom from 
def selectnom():
        curseur = mydb.cursor()
        curseur.execute("SELECT nom FROM test")
        nom = curseur.fetchall()
        return nom

#Create a fonction for select code_postal from 
def selectcode_postal():
        curseur = mydb.cursor()
        curseur.execute("SELECT code_postal FROM test")
        code_postal = curseur.fetchall()
        return code_postal

# Create of window with the class Tk
fenetre = Tk()
fenetre.title("Export - Import ") #Add title 
fenetre.geometry("400x400") #Determined of size

# Create of frame in windows
framea = Frame(fenetre)
framea.pack()


# Add bouton in the frame
boutonImport = Button (framea, text = "Import XML", width= 40, height=1, activebackground='red', command=import_xml)
boutonExport = Button (framea, text = "Export XML", width= 40, height=1, activebackground='red', command=export_xml)
boutonExit = Button (framea, text = "Exit", width= 40, height=1, activebackground='red', command=close_window)
labelfile = Label(framea, text="Nom du fichier", width=40)
entryfile = Entry(framea, text="Nom du fichier", width=40)
labeltable = Label(framea, text="Nom de la table", width=40)
entrytable= Entry(framea, text="table", width=40)
labelImport = Label(framea, text="Import réussi", width=40)
labelExport = Label(framea, text="Export réussi", width=40)




# Add bouton in window
boutonImport.pack(side = TOP)
boutonExport.pack(side = TOP)
boutonExit.pack(side = TOP)
labelfile.pack()
entryfile.pack(side = TOP)
labeltable.pack()
entrytable.pack(side = TOP)
entryfile.focus_force()



# Print of the windows create
fenetre.mainloop()
